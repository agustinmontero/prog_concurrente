import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RdPTest {
    private PipeHTMLParser pipeHTMLParser = new PipeHTMLParser();
    private int[] sensibilizadas= pipeHTMLParser.getSensibilizadas();
    private int[] initialMark = pipeHTMLParser.getInitialMark();
    private int[][] incidenceMatrix = pipeHTMLParser.getIncidenceMatrix(initialMark.length, sensibilizadas.length);
    private Controller controller = new Controller(incidenceMatrix, initialMark, sensibilizadas);

    /**
     * Test para verificar los P-Invariantes de la PN
     * @throws InterruptedException .
     */
    @Test
    void invariantTest() throws InterruptedException {
        controller.iniciarDisparos();
        controller.esperarYTerminar(RdPConstants.TIME_DEFAULT_TEST);
        boolean passInvariant = checkInvariant(controller.getSecuenciaEstados());
        assertTrue(passInvariant);
    }

    /**
     * Test para verificar que no haya ningun numero negativo en la secuencia de estados por la que fue mutando la PN.
     * @throws InterruptedException .
     */
    @Test
    void noNegativeState() throws InterruptedException {
        controller.iniciarDisparos();
        controller.esperarYTerminar(RdPConstants.TIME_DEFAULT_TEST);
        boolean passNoneg = hayNegativos(controller.getSecuenciaEstados());
        assertFalse(passNoneg);
    }

    /**
     * Test para verificar si todos los hilos estan en cola(Deadlock)
     * @throws InterruptedException .
     */
    @Test
    void algunHiloDisponible() throws InterruptedException {
        boolean todosEsperando = false;
        controller.iniciarDisparos();
        controller.esperarYTerminar(RdPConstants.TIME_DEFAULT_TEST);
        if(RdPConstants.MAX_NUM_ACTIVE_THREADS == controller.getCuantosEnCola()){todosEsperando = true;}
        assertFalse(todosEsperando);
    }

    /**
     * Comprueba que en todos los vectores de sensibilizadas, haya al menos una transicion sensibilizada
     * @throws InterruptedException .
     */
    @Test
    void haySensibilizadas() throws InterruptedException {
        controller.iniciarDisparos();
        controller.esperarYTerminar(RdPConstants.TIME_DEFAULT_TEST);
        boolean haySens = hayUnos(controller.getvSensibilizadasList());
        assertTrue(haySens);
    }

    /**
     * Test para verificar la secuencia de disparos realizada por la linea B
     * @throws InterruptedException .
     */
    @Test
    void testTInvariantesLineaB() throws InterruptedException {
        controller.iniciarDisparos();
        controller.esperarYTerminar(RdPConstants.TIME_DEFAULT_TEST);
        boolean errorSecuencia = checkSecuenciaT(RdPConstants.SEC_L_B, controller.getSecuenciaDisparosRdP());
        assertTrue(errorSecuencia);
    }

    @Disabled("Test de politica A desabilitado temporalmente.")
    @Test
    void testPoliticaA() throws InterruptedException {
        controller.iniciarDisparos();
        controller.esperarYTerminar(RdPConstants.TIME_DEFAULT_TEST);
        int[] piezas = controller.getCantidadPiezas();
        System.out.println("Piezas [A,B,C] -> " + Arrays.toString(piezas));
        boolean politicaCumplida = false;
        if((piezas[1] == 2*piezas[0]) && (piezas[1] == 2*piezas[2])){ politicaCumplida = true;}
        assertTrue(politicaCumplida);
    }

    @Disabled("Test de politica B desabilitado temporalmente.")
    @Test
    void testPoliticaB() throws InterruptedException {
        controller.iniciarDisparos();
        controller.esperarYTerminar(RdPConstants.TIME_DEFAULT_TEST);
        int[] piezas = controller.getCantidadPiezas();
        System.out.println("Piezas [A,B,C] -> " + Arrays.toString(piezas));
        boolean politicaCumplida = false;
        if((piezas[2] == 3*piezas[0]) && (piezas[2] == 2*piezas[1])){ politicaCumplida = true;}
        assertTrue(politicaCumplida);
    }

    /**
     * Hace un AND entre el el ultimo vector ocupados de {@link Colas} y el vectoreSensibilizadas de
     * {@link VectorSensibilizadas} para verificar si hay algun hilo en la cola esperando por una transicion sensibilizada.
     * @throws InterruptedException .
     */
    @Disabled("Test temporalmente deshabilitado")
    @Test
    void lastOcupadasAndSensibilizadas() throws InterruptedException {
        controller.iniciarDisparos();
        controller.esperarYTerminar(RdPConstants.TIME_DEFAULT_TEST);
        int [] sens = controller.getvSensibilizadasList();
        int[] ocup = controller.getOcupados();
        System.out.println("SENS ->" + Arrays.toString(sens));
        System.out.println("OCUP ->" + Arrays.toString(ocup));
        System.out.println("EN COLA -> " + controller.getCuantosEnCola());
        boolean sensAndOcup = vAndV(sens, ocup);
        assertTrue(sensAndOcup);
    }

    /**
     * @param estados secuencia de estados por los que fue pasando la RdP
     * @return true si se cumplio con los P-Invariantes, false en caso contrario
     */
    private static boolean checkInvariant(ArrayList<int[]> estados){
        System.out.println("Se van a checkear " + estados.size() + " estados.(invariant)");
        HashMap<int[], Integer> invariantes = RdPInvariants.getPInvariantMap();
        for (Map.Entry<int[],Integer> invariantEntry: invariantes.entrySet()){
            int[] invariantPlaces = invariantEntry.getKey();
            int invariantSum = invariantEntry.getValue();
            for (int[] estado : estados){
                int sumaAcumulada = 0;
                for (int invariantPlace : invariantPlaces) {
                    sumaAcumulada += estado[invariantPlace];
                }
                if(sumaAcumulada != invariantSum){
                    System.out.println("INVARIANTE -> Fallo en el estado: " + estados.indexOf(estado));
                    return  false;}
            }
        }
        return true;
    }

    /**
     * @param estados {@link ArrayList} que contiene todos los estados por los que paso la PN
     * @return true si hay algun numero negativo en algun estado, false si no se encontro ningun numero negativo
     */
    private static boolean hayNegativos(ArrayList<int[]> estados){
        System.out.println("Se van a checkear " + estados.size() + " estados.(noNegativeStates)");
        boolean hayNeg = false;
        for (int[] estado : estados) {
            for (int i : estado) {
                if(i < 0){hayNeg = true;}
            }
        }
        return hayNeg;
    }

    /**
     * @param vectoreSensibilizadasList {@link ArrayList} que contiene todos los vectores de transiciones sensibilizadas
     * @return true si hay al menos una transicion sensibilizada en cada vector, false en caso contrario.
     */
    private static boolean hayUnos(ArrayList<int[]> vectoreSensibilizadasList){
        boolean haySensibilizadas = true;
        for(int[] vSens : vectoreSensibilizadasList){
            boolean sonTodosCero = true;
            for (int i : vSens){
                if(i != 0){sonTodosCero = false;}
            }
            if(sonTodosCero){haySensibilizadas = false;}
        }
        return haySensibilizadas;
    }

    /**
     * @param vectoreSensibilizadas vector con transiciones sensibilizadas
     * @return cantidad de 1's que se encontraron en el vectoreSensibilizadas
     */
    private static boolean hayUnos(int[] vectoreSensibilizadas){
        boolean hayUnos = false;
        for (int i : vectoreSensibilizadas){
            if(i != 0){ hayUnos = true;}
        }
        return hayUnos;
    }

    /**
     * @param sensibilizadas vector de transiciones sensibilizadas
     * @param ocupados vector de los hilos esperando en la cola por una transicion
     * @return true si hay al menos una coincidencia de no-ceros en un mismo indice, false en caso contrario
     * @throws ArrayIndexOutOfBoundsException Si la longitud de los vectores es diferente
     */
    private static boolean vAndV(int[] sensibilizadas, int[] ocupados)throws ArrayIndexOutOfBoundsException{
        if(sensibilizadas.length != ocupados.length){
            throw new ArrayIndexOutOfBoundsException();
        }
        for (int i = 0; i < sensibilizadas.length; i++) {
            if((sensibilizadas[i]  & ocupados[i]) != 0){ return true;}
        }
        return false;
    }

    /**
     * @param vector array del cual se quiere excluir un numero
     * @param nroExcluir numero a excluir del vector
     * @return HashSet que contiene todos los numeros de vector, excepto nroExcluir
     */
    private static HashSet<Integer> getArrayMenosUnElemento(int[] vector, int nroExcluir){
        HashSet<Integer> set = new HashSet<>();
        for(int i : vector){
            if(i != nroExcluir){set.add(i);}
        }
        return set;
    }

    /**
     * @param secuenciaLinea secuencia de disparos de la linea que se quiere realizar la comprobacion
     * @param secuenciaDisparos Secuencia de disparos realizada por la RdP
     * @return true si hubo un fallo en la secuencia de disparos, false en caso contrario.
     */
    private static boolean checkSecuenciaT(int[] secuenciaLinea, ArrayList<Integer> secuenciaDisparos){
        System.out.println("DISPAROS A REVISAR: " + secuenciaDisparos.size());
        Iterator<Integer> iterator = secuenciaDisparos.iterator();
        int counter = 0;
        while (iterator.hasNext()){
            counter++;
            for (int transicionActual : secuenciaLinea) {
                HashSet<Integer> secuenciaMenosI = getArrayMenosUnElemento(secuenciaLinea, transicionActual);
                while (iterator.hasNext()) {
                    int t = iterator.next();
                    if (secuenciaMenosI.contains(t)) {return false;}
                    else if (t == transicionActual) {break;}
                }
            }
        }
        System.out.println(counter + " pasadas.");
        return true;
    }
}