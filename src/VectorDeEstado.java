public class VectorDeEstado {

    private volatile int[] vectorDeEstado;

    VectorDeEstado(int[] vectorDeEstado) {
        this.vectorDeEstado = vectorDeEstado;
    }

    public int[] getVectorDeEstado() {
        return vectorDeEstado;
    }

    /**
     * Calcula el nuevo vector de estado y el vector de las transiciones sensibilizadas, resultante de disparar la
     * transicion nroTransicion
     * @param matrizI .
     * @param vectorSensibilizadas .
     * @param nroTransicion Nro de transicion que se esta queriendo disparar(primera transicion = 0)
     */
    public void calculoDeVectorDeEstado(MatrizI matrizI, VectorSensibilizadas vectorSensibilizadas, int nroTransicion){
        this.calcularProximoEstado(matrizI.getColumna(nroTransicion));
        vectorSensibilizadas.actualiceSensibilizadoT(this.calcularSensibilizadas(matrizI, vectorSensibilizadas.getVectorSensibilizadas().length));

    }

    /**
     * Suma el vector de estado con el vector del parametro
     * @param iXd columna de la matriz I correspondiente a la transicion que se esta queriendo disparar
     * @throws ArrayIndexOutOfBoundsException .
     */
    private void calcularProximoEstado(int[] iXd) throws ArrayIndexOutOfBoundsException{
        if (this.vectorDeEstado.length != iXd.length){
            throw new ArrayIndexOutOfBoundsException();
        }
        for (int i = 0; i < this.vectorDeEstado.length; i++) {
            this.vectorDeEstado[i] += iXd[i];
        }
    }

    /**
     * Suma las columnas de la matriz I con el vector de estado para calcular las transiciones sensibilizadas.
     * @param matrizI Matriz de incidencia
     * @param vSensibilizadasLength tamano del vector sensibilizadas
     * @return vector con las transiciones sensibilizadas
     */
    private int[] calcularSensibilizadas(MatrizI matrizI, int vSensibilizadasLength){
        int[] sensibilizadas = new int[vSensibilizadasLength];

        int[] result = new int[this.vectorDeEstado.length];
        for (int i = 0; i < matrizI.getNroColumnas(); i++) {
            int[] columna = matrizI.getColumna(i);
            for (int j = 0; j < vectorDeEstado.length; j++) {
                result[j] = vectorDeEstado[j] + columna[j];
            }
            sensibilizadas[i] = sonTodosPositivos(result);
        }
        return sensibilizadas;
    }

    /**
     * @param vector Vector resultante de sumar una columna de la matriz I con el vector de estado.
     * @return devuelve 1 si todos los elementos del array son mayor que 0, y devuelve 0 si hay algun numero negativo
     */
    private static int sonTodosPositivos(int[] vector){

        for (int element : vector){
            if(element < 0){
                return 0;
            }
        }
        return 1;
    }
}
