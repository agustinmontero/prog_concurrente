class Accionable {

    private final String mensajeDeAccion;
    private final String id;

    Accionable(String id, String mensajeDeAccion) {
        this.mensajeDeAccion = mensajeDeAccion;
        this.id = id;
    }

    /**
     * Imprime un mensaje con la id del hilo que dispara, la id del {@link Accionable} y el mensajeDeAccion
     * @param idDisparador la id del hilo que esta realizando la accion.
     */
    protected void accionar(String idDisparador){
        //System.out.println("[" + idDisparador+ "] Soy " + id + " - Accion: " + mensajeDeAccion);
    }
}
