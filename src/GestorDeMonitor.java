import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

class GestorDeMonitor {

    private final ReentrantLock bloqueoAcceso;
    private final RdP rdP;
    private Colas colas;
    private final Politicas politicas;
    private final ArrayList<Condition> conditionArrayList;
    private boolean k;

    GestorDeMonitor(int[][] matrizIncidencia, int[] marcadoInicial, int[] vectorSensibilizadas){
        k = true;
        bloqueoAcceso = new ReentrantLock();
        rdP = new RdP(matrizIncidencia, marcadoInicial, vectorSensibilizadas);
        politicas = new Politicas(RdPConstants.relacionPoliticaA);
        colas = new Colas(vectorSensibilizadas.length);
        conditionArrayList = new ArrayList<>();
        for (int ignored : vectorSensibilizadas) {
            conditionArrayList.add(bloqueoAcceso.newCondition());
        }

    }


    /**
     * @param nroTransicion transicion que se quiere disparar
     * @return {@code true} si se pudo disparar la {@link RdP}, {@code false} en caso contrario
     * @throws InterruptedException .
     */
    boolean dispararTransicion(int nroTransicion)throws InterruptedException{
        bloqueoAcceso.lock();
        k = true;
        try {
            while (k){
                k = rdP.disparar(nroTransicion);
                revisarColaDeEspera(nroTransicion, k);
                if(k) {return true;}
                else {
                    if(rdP.isTimed(nroTransicion)){return false;}
                    else {
                        colas.acquire(nroTransicion);
                        this.conditionArrayList.get(nroTransicion).await();
                    }
                }
            }
        } finally {
            bloqueoAcceso.unlock();
        }
        return k;
    }

    /**
     * Revisa en la cola si hay hilos esperando por alguna transicion sensibilizada. Si hay alguno, se le pregunta a la
     * politica quien deberia ser despertado. Si no hay nadie y la red se disparo, actualiza los vectores de la politica.
     * @param nroTransicion transicin que se quiso disparar
     * @param k true si disparo la transicion, false en caso contrario
     */
    private void revisarColaDeEspera(int nroTransicion, boolean k){
        int sensAndQ[] = sensAndQEstan(rdP.getVectorSensibilizadas(), colas.quenesEstan());
        int m = cuantosUnoHay(sensAndQ);
        if (m != 0) {
            int cual = politicas.cual(sensAndQ);
            colas.release(cual);
            this.conditionArrayList.get(cual).signal();
        } else{
            if(k){politicas.actualizarHistorialTransiciones(nroTransicion);}
        }
    }

    /**
     * @param sens vector se transiciones sensibilizadas
     * @param qEstan vector de los que estan esperando en la cola por una trasicion
     * @return Un vector con 1's y 0's: 1 == hay alguien esperando por esa transicion y esta sensibilizada,
     * 0 nadie espera por esa transicion y/o no esta sensibilizada
     * @throws ArrayIndexOutOfBoundsException Si la longitud de sens y qEstan es distinta
     */
    private synchronized static int[] sensAndQEstan(int[] sens, int[] qEstan)throws ArrayIndexOutOfBoundsException{
        if(sens.length != qEstan.length){
            throw new ArrayIndexOutOfBoundsException();
        }
        int[] m = new int[sens.length];
        for (int i = 0; i < sens.length; i++) {
            if(sens[i] == 1 && qEstan[i] == 1){m[i] = 1;}
        }
        return m;
    }

    /**
     * @param sensAndQ vector de enteros del cual se quiere conocer la cantidad de 1's que tiene
     * @return cantidad de 1's encontrados en el vector
     */
    synchronized static int cuantosUnoHay(int[] sensAndQ){
        int counter = 0;
        for (int i : sensAndQ){
            if(i==1){counter++;}
        }
        return counter;
    }

    /**
     * @param v vector
     * @return posicion en la que se encontro el primer 1
     */
    synchronized static int getPosicionPrimerUno(int[] v){
        for (int i = 0; i < v.length; i++) {
            if(v[i] == 1){return i;}
        }
        return -1;
    }

    /**
     * @return Array con la secuencia de disparos que ejecuto la RdP.
     */
    synchronized ArrayList<Integer> getSecuenciaDisparosRdP(){
        return this.rdP.getSecuenciaDisaros();
    }

    /**
     * @return ArrayList con los estados por los que fue pasando la red.
     */
    synchronized ArrayList<int[]> getSecuenciaEstados(){
        return this.rdP.getSecuenciaEstados();
    }


    /**
     * @return Cantidad de {@link Disparador} que estan esperando en la cola
     */
    synchronized int cuantosEnCola(){ return colas.cuantosHay();}

    /**
     * @return vector de transiciones sensibilizadas de la {@link RdP}
     */
    synchronized int[] getvSensibilizadasList(){return rdP.getVectorSensibilizadas();}

    /**
     * @return vector de ocupados de {@link Colas}
     */
    synchronized  int[] getOcupados(){return colas.getOcupado();}

    /**
     * @return vector con el contador de piezas de {@link RdP}
     */
    synchronized int[] getContadorPiezas(){return rdP.getContadorPiezas();}
}
