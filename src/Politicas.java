import java.util.*;

class Politicas {

    private final int[] transitionPriority;
    private int[] historialPoliticas;
    private int[] ultimasPoliticas;
    private int lastIndexSelected;
    private final HashMap<Character, ArrayList<Integer>> lineTransitionsMap;
    private final ArrayList<Character> lineNameLists;
    private final int[] relacionPoliticaEsperada;

    Politicas(int[] relacionPolitica) {
        this.relacionPoliticaEsperada = relacionPolitica;
        transitionPriority = RdPConstants.transitionPriority;
        historialPoliticas = new int[RdPConstants.relacionPoliticaA.length];
        ultimasPoliticas = new int[RdPConstants.relacionPoliticaA.length];
        lastIndexSelected = -1;
        lineTransitionsMap = getLineTransitionsMap();
        lineNameLists = getListName();
    }

    /**
     * @param transiciones vector con transiciones sensibilizadas
     * @return indice de la transicion segun la politica tiene mas prioridad
     */
    synchronized int cual(int[] transiciones){return politica(transiciones);}

    /**
     * @param transiciones array con la las transiciones a seleccionar
     * @return posicion de la transicion con mayor prioridad
     */
    private int getHigherPriorityTransition(int[] transiciones){
        int posicion = -1;
        int max = -1;
        for (int i = 0; i < transiciones.length; i++) {
            if(transiciones[i] == 1){
                if(transitionPriority[i] > max){
                    max = transitionPriority[i];
                    posicion = i;
                }
            }
        }
        return posicion;
    }

    /**
     * @param transiciones vector con transiciones sensibilizadas
     * @return nro de transicion
     */
    private synchronized int politica(int[] transiciones){
        if(GestorDeMonitor.cuantosUnoHay(transiciones) == 1){
            lastIndexSelected = GestorDeMonitor.getPosicionPrimerUno(transiciones);
        } else {
            HashSet<Character> disponibles = getLineasDisponibles(transiciones);
            Character lineSelected = elegirLineaDeProduccion(disponibles);
            int[] transicionesLinea = selectTransicionesDeLinea(lineSelected, transiciones);
            lastIndexSelected = getHigherPriorityTransition(transicionesLinea);
        }
        actualizarHistorialTransiciones(lastIndexSelected);
        comprobarLimitePolitica();
        return  lastIndexSelected;
    }

    /**
     * @return HashMap que contiene: K = un caracter indicando la linea de produccion, V = transiciones correspondientes
     * a la linea de produccion.
     */
    private static HashMap<Character,ArrayList<Integer>> getLineTransitionsMap(){
        HashMap<Character,ArrayList<Integer>> lineTrMap = new HashMap<>();
        ArrayList<Integer> transitionsLA = new ArrayList<>();
        for(int i : RdPConstants.SEC_L_A1){
            transitionsLA.add(i);
        }
        for(int i : RdPConstants.SEC_L_A2){
            if(transitionsLA.contains(i)){continue;}
            transitionsLA.add(i);
        }
        lineTrMap.put(RdPConstants.LINEA_A_NAME, transitionsLA);

        ArrayList<Integer> transitionsLB = new ArrayList<>();
        for(int i: RdPConstants.SEC_L_B){
            transitionsLB.add(i);
        }
        lineTrMap.put(RdPConstants.LINEA_B_NAME, transitionsLB);

        ArrayList<Integer> transitionsLC = new ArrayList<>();
        for(int i : RdPConstants.SEC_L_C){
            transitionsLC.add(i);
        }
        lineTrMap.put(RdPConstants.LINEA_C_NAME, transitionsLC);

        return lineTrMap;
    }

    /**
     * @return ArrayList con los nombres de las lineas de produccion
     */
    private static ArrayList<Character> getListName(){
        ArrayList<Character> nameList = new ArrayList<>();
        for(char c : RdPConstants.LINES_NAME){
            nameList.add(c);
        }
        return nameList;
    }

    /**
     * Pone a cero el vector ultimasPoliticas
     */
    private void resetUltimaPolitica(){
        for (int i = 0; i < ultimasPoliticas.length; i++) {
            ultimasPoliticas[i] = 0;
        }
    }

    /**
     * Actualiza los arrays historialPoliticas u ultimasPoliticas.
     * @param transition nro de transicion
     */
    synchronized void actualizarHistorialTransiciones(int transition){
        for(Map.Entry<Character, ArrayList<Integer>> entry : lineTransitionsMap.entrySet()){
            if(entry.getValue().contains(transition)){
                int index = lineNameLists.indexOf(entry.getKey());
                historialPoliticas[index]++;
                ultimasPoliticas[index]++;
                break;
            }
        }
    }

    /**
     * Funcion que verifica si se ha excedido una determinada cantidad de veces el limite de eleccion de una linea en
     * particualr. En caso de que se haya excedido, resetea el arreglo de ultimasPoliticas
     */
    private void comprobarLimitePolitica(){
        for (int i = 0; i < ultimasPoliticas.length; i++) {
            if(ultimasPoliticas[i] > (relacionPoliticaEsperada[i] + RdPConstants.LIMITE_RESET_POLITICA)){
                resetUltimaPolitica();
                break;
            }
        }
    }

    /**
     * @param transiciones arreglo con transiciones sensibilizadas( 1 = sensibilizada, 0 = no_sensibilizada)
     * @return HashSet que contiene las Lineas disponibles
     */
    private HashSet<Character> getLineasDisponibles(int[] transiciones){
        HashSet<Character> lineasDisponibles = new HashSet<>();
        for (int i = 0; i < transiciones.length; i++) {
            for (Map.Entry<Character,ArrayList<Integer>> entry : lineTransitionsMap.entrySet()){
                if(transiciones[i] == 1 && entry.getValue().contains(i)){
                    lineasDisponibles.add(entry.getKey());
                    break;
                }
            }
        }
        return lineasDisponibles;
    }

    /**
     * @param lineSet Conjunto que contiene las lineas de produccion que tienen transiciones sensibilizadas
     * @return Linea de produccion elegida segun la relacion de politica actual
     */
    private Character elegirLineaDeProduccion(HashSet<Character> lineSet){
        HashMap<Character, Integer> diferenciaMap = new HashMap<>();
        for(Character line : lineSet){
            int i = lineNameLists.indexOf(line);
            diferenciaMap.put(line, relacionPoliticaEsperada[i] - ultimasPoliticas[i]);
        }
        int maxDiferencia = Integer.MIN_VALUE;
        Character selectedLine = null;
        for (Map.Entry<Character,Integer> entry : diferenciaMap.entrySet()){
            if(entry.getValue() > maxDiferencia){
                maxDiferencia = entry.getValue();
                selectedLine = entry.getKey();
            }
        }
        return selectedLine;
    }

    /**
     * @param line linea de la que se quiere obtener las transiciones sensibilizadas
     * @param transiciones vector con las transiciones sensibilizadas
     * @return vector con las transiciones sensibilizadas de la linea de produccion elegida
     */
    private int[] selectTransicionesDeLinea(Character line, int[] transiciones){
        int[] transicionesDeLinea = new int[transiciones.length];
        for (int i = 0; i < transiciones.length; i++) {
            if(transiciones[i] == 1 && lineTransitionsMap.get(line).contains(i)){
                transicionesDeLinea[i] = 1;
            }
        }
        return transicionesDeLinea;
    }
}
