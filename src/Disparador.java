import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

class Disparador implements Runnable{

    private final GestorDeMonitor gestorDeMonitor;
    private final AtomicBoolean running;
    private final int[] secuenciaDisparo;
    private int posicionActual;
    private int cycleCounter;
    private final String idDisparador;
    private final ConcurrentHashMap<String,Integer> piezasPorThreadMap;
    private final ConcurrentHashMap<Integer, Accionable> accionableMap;

    Disparador(GestorDeMonitor gestorDeMonitor, AtomicBoolean running, int[] secuenciaDisparo, String idDisparador,
               ConcurrentHashMap<String, Integer> piezasPorThreadMap, ConcurrentHashMap<Integer, Accionable> accionableMap) {
        this.gestorDeMonitor = gestorDeMonitor;
        this.running = running;
        this.secuenciaDisparo = secuenciaDisparo;
        this.piezasPorThreadMap = piezasPorThreadMap;
        this.accionableMap = accionableMap;
        this.posicionActual = 0;
        this.cycleCounter = 0;
        this.idDisparador = idDisparador;
    }

    @Override
    public void run(){
        boolean k;
        try {
            while (running.get()){
                k = gestorDeMonitor.dispararTransicion(secuenciaDisparo[posicionActual]);
                if(k){
                    checkForActions(secuenciaDisparo[posicionActual]);
                    updatePosicionActualYCiclos();
                }
            }
        } catch (InterruptedException ignored){}
        finally {
            piezasPorThreadMap.put(idDisparador, cycleCounter);
            System.out.println("Soy " + this.idDisparador + " e hice " + cycleCounter + " piezas.");
        }
    }

    /**
     * Actualiza la posicion dentro del arreglo secuenciaDisparo y el contador de ciclos
     */
    private void updatePosicionActualYCiclos(){
        this.posicionActual++;
        if(this.posicionActual >= this.secuenciaDisparo.length){
            this.posicionActual = 0;
            this.cycleCounter++;
        }
    }

    private void checkForActions(int nroTransicion){
        if(accionableMap.containsKey(nroTransicion)){
            accionableMap.get(nroTransicion).accionar(idDisparador);
        }
    }
}
