public class MatrizI {

    private final int matrizI[][];

    MatrizI(int[][] matrizI) {
        this.matrizI = matrizI;
    }

    int[] getColumna(int nroCol)throws ArrayIndexOutOfBoundsException{
        if(nroCol >= matrizI[0].length) {
            throw new ArrayIndexOutOfBoundsException();
        }

        int[] columna = new int[matrizI.length];

        for (int i = 0; i < matrizI.length; i++) {
            columna[i] = matrizI[i][nroCol];
        }
        return columna;
    }

    /**
     * @return numero de columnas de la matriz I(matriz de incidencia)
     */
    int getNroColumnas(){
        return this.matrizI[0].length;
    }
}
