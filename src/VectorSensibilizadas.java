public class VectorSensibilizadas {

    private volatile int[] vectorSensibilizadas;
    private final SensibilizadoConTiempo sensibilizadoConTiempo;

    VectorSensibilizadas(int[] vectorSensibilizadas) {
        this.vectorSensibilizadas = vectorSensibilizadas;
        sensibilizadoConTiempo = new SensibilizadoConTiempo(RdPConstants.ALFA, RdPConstants.BETA);
    }

    int[] getVectorSensibilizadas() {return vectorSensibilizadas;}

    private void setVectorSensibilizadas(int[] vectorSensibilizadas) throws ArrayIndexOutOfBoundsException {
        if(vectorSensibilizadas.length > this.vectorSensibilizadas.length){
            throw new ArrayIndexOutOfBoundsException();
        }
        this.vectorSensibilizadas = vectorSensibilizadas;
    }

    synchronized boolean estaSensibilizado(int nroTransicion){
        //return vectorSensibilizadas[nroTransicion] == 1;
        if(!sensibilizadoConTiempo.isTimed(nroTransicion)){return vectorSensibilizadas[nroTransicion] == 1;}
        else {
            if(vectorSensibilizadas[nroTransicion] == 1){
                boolean ventana, esperando;
                ventana = sensibilizadoConTiempo.testVentanaTiempo(nroTransicion);
                if (ventana){
                    esperando = sensibilizadoConTiempo.getEstaEsperando(nroTransicion);
                    if(esperando){return false;}
                    else {
                        sensibilizadoConTiempo.setNuevoTimeStamp(nroTransicion);
                        return true;
                    }
                } else {
                    //System.out.println("Fuera de ventana.");
                    return false;
                }
            } else {return false;}
        }

    }

    synchronized void actualiceSensibilizadoT(int[] newVector){
        setVectorSensibilizadas(newVector);
        for (int i = 0; i < newVector.length; i++) {
            if(newVector[i] == 1 && sensibilizadoConTiempo.isTimed(i)){
                sensibilizadoConTiempo.setNuevoTimeStamp(i);
            }
        }
    }

    synchronized void resetEsperando(int nroTransicion){
        if(sensibilizadoConTiempo.isTimed(nroTransicion)){sensibilizadoConTiempo.resetEsperando(nroTransicion);}
    }

    synchronized void setNuevoTimeStamp(int nroTransicion){
        if(sensibilizadoConTiempo.isTimed(nroTransicion)){sensibilizadoConTiempo.setNuevoTimeStamp(nroTransicion);}
    }

    synchronized boolean isTimed(int nroTransicion){return sensibilizadoConTiempo.isTimed(nroTransicion);}
}
