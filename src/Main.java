class Main {

    public static void main(String[] args) {
        PipeHTMLParser pipeHTMLParser = new PipeHTMLParser();
        int[] sensibilizadas= pipeHTMLParser.getSensibilizadas();
        int[] initialMark = pipeHTMLParser.getInitialMark();
        int[][] incidenceMatrix = pipeHTMLParser.getIncidenceMatrix(initialMark.length, sensibilizadas.length);
        //System.out.println("Mapa de transiciones:\n" + pipeHTMLParser.getTransitionMap());
        Controller controller = new Controller(incidenceMatrix, initialMark, sensibilizadas);
        controller.iniciarDisparos();
        try {
            controller.esperarYTerminar(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Piezas/thread: \n" + controller.getPiezasPorThreadMap());
        System.out.println("TERMINANDO...");
    }
}
