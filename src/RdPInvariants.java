import java.util.HashMap;

public class RdPInvariants {

    /**
     * @return HashMap con los P-Invariantes de la red. Key = vector con los numeros de plazas, Value = invariante de
     * esas plazas
     */
    public static HashMap<int[],Integer> getPInvariantMap(){
        HashMap<int[],Integer> invariant = new HashMap<>();
        invariant.put(new int[]{0,7}, 1);
        invariant.put(new int[]{1,11,16}, 1);
        invariant.put(new int[]{2,8,22}, 1);
        invariant.put(new int[]{3,12,20}, 1);
        invariant.put(new int[]{4,5,6,7,8,9,10,11,12,13}, 3);
        invariant.put(new int[]{14,15,16,17}, 2);
        invariant.put(new int[]{18,19,20,21,22,23}, 3);
        invariant.put(new int[]{5,23,24}, 1);
        invariant.put(new int[]{9,10,15,17,21,25}, 1);
        invariant.put(new int[]{13,19,26}, 1);
        invariant.put(new int[]{9,11,15,16,27}, 1);
        invariant.put(new int[]{8,10,12,19,20,21,28}, 1);
        return invariant;
    }
}
