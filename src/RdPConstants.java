import java.util.concurrent.ConcurrentHashMap;

class RdPConstants {
    public static final int MAX_NUM_ACTIVE_THREADS = 8;

    public static final String HTML_FILE_PATHNAME = "rdp_files/inc_and_mark_ej_1.html";
    public static final int TABLE_INCIDENSE_MATRIX_INDEX = 2;
    public static final int TABLE_MARKING_INDEX= 4;
    public static final int TABLE_ENABLED_TRANSITIONS_INDEX = 5;

    public static final int[] SEC_L_A1 = {0,2,1,5,7,9,11};
    public static final int[] SEC_L_A2 = {0,3,4,6,8,10,11};
    public static final int[] SEC_L_B = {12,13,14,15};
    public static final int[] SEC_L_C = {16,17,18,19,20,21};

    public static final int N_THREADS_L_A1 = 2;
    public static final int N_THREADS_L_A2 = 1;
    public static final int N_THREADS_L_B = 2;
    public static final int N_THREADS_L_C = 3;

    public static final String THREAD_BASE_NAME_LINEA_A1 = "T_L_A1-";
    public static final String THREAD_BASE_NAME_LINEA_A2 = "T_L_A2-";
    public static final String THREAD_BASE_NAME_LINEA_B = "T_L_B-";
    public static final String THREAD_BASE_NAME_LINEA_C = "T_L_C-";

    public static final int TIMED_TRANSITIONS[]={0,0,0,0,0,1,1,0,0,1,1,0,0,0,1,0,0,0,1,0,1,0};
    public static final int[] REAL_TIMES = {0,0,0,0,0,30,24,0,0,5,10,0,0,0,15,0,0,0,21,0,18,0};
    public static final long ALFA[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    public static final long BETA[]={0,0,0,0,0,600,480,0,0,100,200,0,0,0,300,0,0,0,420,0,360,0};

    public static final int[] transitionPriority = {10,11,12,13,14,15,16,17,18,19,20,21,2,3,4,5,26,27,28,29,30,31};

    public static final int[] relacionPoliticaA = {7000,8000,6000};//{A,B,C}
    public static final int[] relacionPoliticaB = {36,8,6};//{A,B,C}

    public static final char LINEA_A_NAME = 'A';
    public static final char LINEA_B_NAME = 'B';
    public static final char LINEA_C_NAME = 'C';
    public static final char[] LINES_NAME = {LINEA_A_NAME, LINEA_B_NAME, LINEA_C_NAME};

    public static final int LIMITE_RESET_POLITICA = 1000;

    public static final int TIME_DEFAULT_TEST = 20;

    private static final String ROBOT_1_ID = "Robot_1";
    private static final String ROBOT_2_ID = "Robot_2";
    private static final String ROBOT_3_ID = "Robot_3";

    private static final String MAQUINA_1_ID = "Maquina_1";
    private static final String MAQUINA_2_ID = "Maquina_2";
    private static final String MAQUINA_3_ID = "Maquina_3";
    private static final String MAQUINA_4_ID = "Maquina_4";


    /**
     * Genera el {@link ConcurrentHashMap} que se le va a pasar a todas las instancias {@link Disparador}
     * @return Mapa con clave(K) = numero de transicion, valor(V) = {@link Accionable}
     */
    static ConcurrentHashMap<Integer, Accionable> getAccionablesMap(){
        ConcurrentHashMap<Integer, Accionable> accionableMap = new ConcurrentHashMap<>();

        accionableMap.put(0, new Robot(ROBOT_1_ID, "llevar pieza a maquina."));
        accionableMap.put(1, new Maquina(MAQUINA_1_ID, "hacer agujero."));
        accionableMap.put(5, new Robot(ROBOT_2_ID, "llevar pieza a " + MAQUINA_2_ID));
        accionableMap.put(7, new Maquina(MAQUINA_2_ID, "poner remaches."));
        accionableMap.put(9, new Robot(ROBOT_3_ID, "llevar producto terminado a salida linea A."));
        accionableMap.put(11, new Robot(ROBOT_3_ID, "producto de linea A terminado."));
        accionableMap.put(4, new Maquina(MAQUINA_3_ID, "hacer agujero."));
        accionableMap.put(6, new Robot(ROBOT_2_ID, "llevar pieza a " + MAQUINA_4_ID));
        accionableMap.put(8, new Maquina(MAQUINA_4_ID, "poner remaches."));
        accionableMap.put(10, new Robot(ROBOT_3_ID, "llevar producto terminado a salida linea A."));

        accionableMap.put(12, new Robot(ROBOT_2_ID, "llevar pieza a " + MAQUINA_2_ID));
        accionableMap.put(13, new Maquina(MAQUINA_2_ID, "hacer marca de agua."));
        accionableMap.put(14, new Robot(ROBOT_2_ID, "llevar pieza a salida linea B."));
        accionableMap.put(15, new Robot(ROBOT_2_ID, "producto de linea B terminado."));

        accionableMap.put(16, new Robot(ROBOT_3_ID, "llevar pieza a " + MAQUINA_4_ID));
        accionableMap.put(17, new Maquina(MAQUINA_4_ID, "calentar pieza."));
        accionableMap.put(18, new Robot(ROBOT_2_ID, "llevar pieza a " + MAQUINA_3_ID));
        accionableMap.put(19, new Maquina(MAQUINA_3_ID, "alizar pieza."));
        accionableMap.put(20, new Robot(ROBOT_1_ID, "llevar pieza a salida linea C."));
        accionableMap.put(21, new Robot(ROBOT_1_ID, "producto de linea C terminado."));

        return  accionableMap;
    }
}
