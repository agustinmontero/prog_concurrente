class SensibilizadoConTiempo {

    private int[] esperando;
    private final long[] alfa;
    private final long[] beta;
    private long[] timeStamp;
    private final int[] isTimed;

    SensibilizadoConTiempo(long[] alfa, long[] beta) {
        this.alfa = alfa;
        this.beta = beta;
        this.esperando = new int[alfa.length];
        this.timeStamp = new long[alfa.length];
        isTimed = RdPConstants.TIMED_TRANSITIONS;
        for (int i = 0; i < timeStamp.length; i++) {
            setNuevoTimeStamp(i);
        }
    }

    /**
     * @param nroTransicion transicion que se quiere realizar el testeo de la ventana de tiempo
     * @return true si esta dentro de la ventana de tiempo, false en caso contrario
     */
    boolean testVentanaTiempo(int nroTransicion){
        long ventana = getVentanaTiempo(nroTransicion);
        return (ventana >= this.alfa[nroTransicion]) && (ventana <= this.beta[nroTransicion]);
    }

    /**
     * @param nroTransicion transicion que se quiere actualizar el timeStamp
     */
    void setNuevoTimeStamp(int nroTransicion){
        this.timeStamp[nroTransicion] = System.currentTimeMillis();
    }

    boolean antesDeLaVentana(int transicion){
        return getVentanaTiempo(transicion) < alfa[transicion];
    }

    void setEsperando(int transicion){
        this.esperando[transicion] = 1;
    }

    /**
     * @param nroTransicion transicion que se quiere poner a cero en el vectro esperando
     */
    void resetEsperando(int nroTransicion){
        this.esperando[nroTransicion] = 0;
    }

    /**
     * @param nroTransicion .
     * @return true si hay alguien esperando por la transicion, false en caso contrario
     */
    boolean getEstaEsperando(int nroTransicion){
        return esperando[nroTransicion] == 1;
    }

    /**
     * @param transicion transicion de la cual se quiere saber la ventana de tiempo
     * @return diferencia de tiempo entre ahora y el timeStamp de la transicion
     */
    private long getVentanaTiempo(int transicion){
        return System.currentTimeMillis() - timeStamp[transicion];
    }

    /**
     * @param nroTransicion transicion de la cual se quiere saber si es temporizada o no
     * @return true si la transicion es temporizada, false en caso contrario
     */
    boolean isTimed(int nroTransicion){return this.isTimed[nroTransicion] == 1;}
}
