import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class PipeHTMLParser {
    private Document document;

    PipeHTMLParser() {
        try {
            File file = new File(RdPConstants.HTML_FILE_PATHNAME);
            document = Jsoup.parse(file, "UTF-8");
        } catch (IOException e ) {
            e.printStackTrace();
            System.exit(2);
        }
    }

    /**
     * @param pathName ruta al archivo HTML
     */
    PipeHTMLParser(String pathName) {
        try {
            File file = new File(pathName);
            document = Jsoup.parse(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param index El indice de la tabla que se quiere obtener(comenzando desde 0)
     * @return Arrya que contiene los elementos de la tabla
     */
    private ArrayList<String> getArrayFromHTMLTable(int index){
        ArrayList<String> arrayList = new ArrayList<>();
        Element table = document.select("table table").get(index);

        for (Element row: table.select("tr")) {
            for(Element element: row.select("td")){
                if(element.text().equals("")){continue;}
                arrayList.add(element.text());
            }
        }
        return arrayList;
    }

    /**
     * @param numRows Cantidad de filas de la matriz de I
     * @param numCols Cantidad de columnas de la matriz I
     * @return Array 2D con la matriz I
     */
    int[][] getIncidenceMatrix(int numRows, int numCols){
        Element table = document.select("table table").get(RdPConstants.TABLE_INCIDENSE_MATRIX_INDEX);
        Elements rows = table.select("tr");
        int[][] incience = new int[numRows][numCols];
        for (int i = 1; i < rows.size(); i++) {
            Elements row = rows.get(i).select("td");
            for (int j = 1; j < row.size(); j++) {
                incience[i-1][j-1] = Integer.parseInt(row.get(j).text());
            }
        }
        return incience;
    }

    /**
     * @return Vector de transiciones sensibilizadas que contiene 1's y 0's: 1 transicion sensibilizada,
     * 0 no sensibilizada
     */
    int[] getSensibilizadas(){
        Element table = document.select("table table").get(RdPConstants.TABLE_ENABLED_TRANSITIONS_INDEX);
        Elements rows = table.select("tr");
        ArrayList<Integer> sensibilizadas = new ArrayList<>();
        for(Element element : rows){
            Elements row = element.select("td");
            for(Element a : row){
                if(!a.text().equals("yes") && !a.text().equals("no")){continue;}
                if(a.text().equals("yes")){sensibilizadas.add(1);}
                else {sensibilizadas.add(0);}
            }
        }
        return toIntArray(sensibilizadas);
    }

    /**
     * @return Devuelve un {@link ArrayList} que contiene las etiquetas de las transiciones(en el orden que se leyeron)
     */
    private ArrayList<String> getTransitionsTag(){
        ArrayList<String> tagList = new ArrayList<>();
        Element table = document.select("table table").get(RdPConstants.TABLE_ENABLED_TRANSITIONS_INDEX);
        Elements rows = table.select("tr");
        for(Element element : rows){
            Elements row = element.select("td");
            for(Element a : row){
                if(a.text().equals("yes") || a.text().equals("no")){continue;}
                tagList.add(a.text());
            }
        }
        return tagList;
    }

    /**
     * @return Vector que contiene el marcado inicial de la PN
     */
    int[] getInitialMark(){
        Element table = document.select("table table").get(RdPConstants.TABLE_MARKING_INDEX);
        Elements rows = table.select("tr");
        ArrayList<Integer> initialMark = new ArrayList<>();
        for(Element element : rows){
            Elements row = element.select("td");
            if(row.get(0).text().equals("Initial")){
                for(Element a : row.subList(1, row.size())){
                    initialMark.add(Integer.parseInt(a.text()));
                }
                return toIntArray(initialMark);
            }
        }
        return null;
    }

    /**
     * @param arrayList Array que se quiere convertir a int[]
     * @return vectro de int que contiene los elementos de arrayList
     */
    private static int[] toIntArray(ArrayList<Integer> arrayList){
        int[] intArray = new int[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            intArray[i] = arrayList.get(i);
        }
        return intArray;
    }

    /**
     * @return Devuelve un mapa con: clave(k) = nro de transicion, valor(V) = etiqueta de la transcion
     */
    HashMap<Integer,String> getTransitionMap(){
        HashMap<Integer,String> transitionMap = new HashMap<>();
        ArrayList<String> transitionsList = this.getTransitionsTag();
        for(String transition : transitionsList){
            transitionMap.put(transitionsList.indexOf(transition), transition);
        }
        return transitionMap;
    }
}
