import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class Controller {

    private final GestorDeMonitor gestorDeMonitor;
    private final ThreadPoolExecutor executor;
    private volatile AtomicBoolean running;
    private boolean started;
    private final ConcurrentHashMap<String, Integer> piezasPorThreadMap;
    private final ConcurrentHashMap<Integer, Accionable> accionableMap;

    Controller(int[][] matrizIncidencia, int[] marcadoInicial, int[] vectorSensibilizadas) {
        running = new AtomicBoolean(false);
        gestorDeMonitor = new GestorDeMonitor(matrizIncidencia, marcadoInicial, vectorSensibilizadas);
        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(RdPConstants.MAX_NUM_ACTIVE_THREADS);
        started = false;
        piezasPorThreadMap = new ConcurrentHashMap<>();
        accionableMap = RdPConstants.getAccionablesMap();
    }

    /**
     * Comprueba que el controlador no haya sido iniciado previamente. Si no fue iniciado, agrega los objetos Runnable
     * al executror, tantos como esten definido en {@link RdPConstants} y luego lo apaga para que no se sigan agregando
     * tareas. Si ya fue iniciado, no hace nada.
     */
    synchronized void iniciarDisparos(){
        if(!started){
            started = true;
            running.set(true);
            HashSet<Disparador> disparadors = this.getTheRunners();
            for (Disparador disparador : disparadors){
                executor.execute(disparador);
            }
            executor.shutdown();
        }
    }

    /**
     * @return El estado de la variable running
     */
    synchronized boolean isRunning(){return running.get();}

    /**
     * Setea la variable running en false y llama al metodo shutdownNow de executor
     * @param tiempo cantidad de segundos a esperar por la finalizacion de la ejecucion
     * @throws InterruptedException x
     */
    synchronized void esperarYTerminar(long tiempo) throws InterruptedException {
        this.executor.awaitTermination(tiempo, TimeUnit.SECONDS);
        detenerDisparos();
        Thread.sleep(1000);
        this.executor.shutdownNow();
        Thread.sleep(1000);
    }

    /**
     * Llama al metodo shutdownNow de executor
     */
    void detenerExecutor(){this.executor.shutdownNow();}

    private synchronized void detenerDisparos(){this.running.set(false);}

    /**
     * @return Array que contiene los objetos {@link Disparador} de las lineas de produccion
     */
    private synchronized HashSet<Disparador> getTheRunners(){
        HashSet<Disparador> disparadores = new HashSet<>();
        for (int i = 0; i < RdPConstants.N_THREADS_L_A1; i++) {
            disparadores.add(new Disparador(gestorDeMonitor, running, RdPConstants.SEC_L_A1,
                    RdPConstants.THREAD_BASE_NAME_LINEA_A1 + i, piezasPorThreadMap, accionableMap));
        }

        for (int i = 0; i < RdPConstants.N_THREADS_L_A2; i++) {
            disparadores.add(new Disparador(gestorDeMonitor, running, RdPConstants.SEC_L_A2,
                    RdPConstants.THREAD_BASE_NAME_LINEA_A2 + i, piezasPorThreadMap, accionableMap));
        }

        for (int i = 0; i < RdPConstants.N_THREADS_L_B; i++) {
            disparadores.add(new Disparador(gestorDeMonitor, running, RdPConstants.SEC_L_B,
                    RdPConstants.THREAD_BASE_NAME_LINEA_B + i, piezasPorThreadMap, accionableMap));
        }

        for (int i = 0; i < RdPConstants.N_THREADS_L_C; i++) {
            disparadores.add(new Disparador(gestorDeMonitor, running, RdPConstants.SEC_L_C,
                    RdPConstants.THREAD_BASE_NAME_LINEA_C + i, piezasPorThreadMap, accionableMap));
        }
        return disparadores;
    }

    /**
     * @return ArrayList que contiene todos los estados por los que fue pasando la red(en orden)
     */
    synchronized ArrayList<int[]> getSecuenciaEstados(){return this.gestorDeMonitor.getSecuenciaEstados();}

    /**
     * @return Cantidad de hilos que estan esperando en la cola por una transicion
     */
    synchronized int getCuantosEnCola(){ return gestorDeMonitor.cuantosEnCola();}

    /**
     * @return mapa que contiede k = Thread_ID, V = cnatidad_piezas_realizadas
     */
    synchronized ConcurrentHashMap<String, Integer> getPiezasPorThreadMap() {return piezasPorThreadMap;}

    /**
     * @return vector con las transiciones sensibilizadas(1 = sensibilizada, 0 caso contrario)
     */
    synchronized int[] getvSensibilizadasList(){ return gestorDeMonitor.getvSensibilizadasList();}

    /**
     * @return vector de cola, que indica cuantos hilos hay esperando por cada transicion
     */
    synchronized  int[] getOcupados(){return gestorDeMonitor.getOcupados();}

    /**
     * @return vector que contiene la cantidad de piezas que se fabrico en cada linea(A,B,C,...)
     */
    synchronized int[] getCantidadPiezas(){return gestorDeMonitor.getContadorPiezas();}

    synchronized ArrayList<Integer> getSecuenciaDisparosRdP(){return gestorDeMonitor.getSecuenciaDisparosRdP();}
}
