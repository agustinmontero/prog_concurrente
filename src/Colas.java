import java.util.concurrent.locks.ReentrantLock;

public class Colas{

    private int[] ocupado;
    private final ReentrantLock lock;

    Colas(int nroTransiciones) {
        ocupado = new int[nroTransiciones];
        lock = new ReentrantLock();
    }

    /**
     * @return Array qEstan: contiene 1 para las transiciones que estan en espera, 0 para las que no estan esperando.
     */
    int[] quenesEstan(){
        lock.lock();
        try {
            int[] qEstan = new int[ocupado.length];
            for (int i = 0; i < ocupado.length; i++) {
                if(ocupado[i] > 0){qEstan[i] = 1;}
            }
            return qEstan;
        } finally {
            lock.unlock();
        }
    }

    /**
     * @param nroTransicion Numero de transicion que se quiere poner a la espera
     */
    void acquire(int nroTransicion) {
        lock.lock();
        try {
            ocupado[nroTransicion] += 1;
        } finally {
            lock.unlock();
        }
    }

    /**
     * @param nroTransicion Numero de transicion que se quiere reactivar
     */
    void release(int nroTransicion) {
        lock.lock();
        try {
            ocupado[nroTransicion] -= 1;
        } finally {
            lock.unlock();
        }
    }

    /**
     * @return Cantidad de hilos esperando en la cola
     */
    int cuantosHay(){
        lock.lock();
        int cuantos = 0;
        try {
            for (int anOcupado : ocupado) {
                cuantos += anOcupado;
            }
        } finally {
            lock.unlock();
        }
        return cuantos;
    }

    /**
     * @return vector de ocupados en la cola
     */
    int[] getOcupado() {return ocupado;}
}
