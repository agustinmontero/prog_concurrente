import java.util.ArrayList;

public class RdP {

    private final MatrizI matrizI;
    private final VectorDeEstado vectorDeEstado;
    private final VectorSensibilizadas vectorSensibilizadas;
    private final ArrayList<Integer> secuenciaDisaros;
    private final ArrayList<int[]> secuenciaEstados;
    private int[] contadorPiezas;

    RdP(int[][] matrizI, int[] vectorDeEstado, int[] vectorSensibilizadas) {
        this.matrizI = new MatrizI(matrizI);
        this.vectorDeEstado = new VectorDeEstado(vectorDeEstado);
        this.vectorSensibilizadas = new VectorSensibilizadas(vectorSensibilizadas);
        secuenciaDisaros = new ArrayList<>();
        secuenciaEstados = new ArrayList<>();
        contadorPiezas = new int[RdPConstants.LINES_NAME.length];
    }

    /**
     * @param nroTransicion transicion que se quiere disparar
     * @return true si el disparo se pudo realizar, false en caso contrario
     */
    synchronized boolean disparar(int nroTransicion){
        boolean k = this.vectorSensibilizadas.estaSensibilizado(nroTransicion);
        if(k){
            vectorDeEstado.calculoDeVectorDeEstado(this.matrizI, this.vectorSensibilizadas, nroTransicion);
            secuenciaDisaros.add(nroTransicion);
            secuenciaEstados.add(this.vectorDeEstado.getVectorDeEstado());
            vectorSensibilizadas.resetEsperando(nroTransicion);
            vectorSensibilizadas.setNuevoTimeStamp(nroTransicion);
            actualizarPiezas(nroTransicion);
        }
        return k;
    }

    /**
     * @return vector con las transiciones sensibilizadas
     */
    synchronized int[] getVectorSensibilizadas() {return vectorSensibilizadas.getVectorSensibilizadas();}

    /**
     * @return ArrayList que contiene la secuencia de disparos que realizo la RdP
     */
    synchronized ArrayList<Integer> getSecuenciaDisaros() {return secuenciaDisaros;}

    /**
     * @return ArrayList con la secuenciaEstados de la RdP
     */
    synchronized ArrayList<int[]> getSecuenciaEstados() {return secuenciaEstados;}

    /**
     * @param nroTransicion transicion de la que se quiere saber si es temporizada o no
     * @return true si la transicion es temporizada, false en caso contrario
     */
    synchronized boolean isTimed(int nroTransicion){return vectorSensibilizadas.isTimed(nroTransicion);}

    /**
     * Actualiza la cantidad de piezas en el contador de piezas luego de haber disparado una transicion. Si la
     * transicion disparada es la correspondiente a la ultima en alguna secuencia de disparo, agrega uno.
     * @param nroTransicion transicion disparada
     */
    private synchronized void actualizarPiezas(int nroTransicion){
        switch (nroTransicion){
            case 11: contadorPiezas[0]++;
            break;
            case 15: contadorPiezas[1]++;
            break;
            case 21: contadorPiezas[2]++;
            default: break;
        }
    }

    /**
     * @return vector con las piezas que se hicieron por linea de produccion [A,B,...]
     */
    synchronized int[] getContadorPiezas() {return contadorPiezas;}
}
